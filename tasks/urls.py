from django.urls import path
from tasks.views import create_task
from projects.views import list_projects

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("tasks/create/", create_task, name="create_task"),
]
